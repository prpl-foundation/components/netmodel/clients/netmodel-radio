# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.1.2 - 2022-12-21(12:42:43 +0000)

### Other

- Opensource component

## Release v0.1.1 - 2022-10-06(08:37:56 +0000)

### Fixes

- [Netmodel] remove lowerlayers parameter from ssid and radio

## Release v0.1.0 - 2022-08-25(07:51:16 +0000)

### New

- [netmodel][Wifi SSID] Implement a Radio WiFi client in netmodel

